<?php

/**
 * @file
 * Settings applied to our Lando environment.
 */

// Database connection settings.
$databases['default']['default'] = [
  'database' => 'drupal8',
  'username' => 'drupal8',
  'password' => 'drupal8',
  'prefix' => '',
  'host' => 'database',
  'port' => '3306',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
];

// Ensure verbose error reporting.
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
$config['system.logging']['error_level'] = 'verbose';

// Skip file system permissions hardening.
$settings['skip_permissions_hardening'] = TRUE;

// Enable local development services.
$settings['container_yamls'][] = __DIR__ . '/../services--non-scaffolded/development.services.yml';

// Disable CSS and JS aggregation.
$config['system.performance']['css']['preprocess'] = FALSE;
$config['system.performance']['js']['preprocess'] = FALSE;

// Disable the render cache (this includes the page cache).
$settings['cache']['bins']['render'] = 'cache.backend.null';

// Disable Dynamic Page Cache.
$settings['cache']['bins']['dynamic_page_cache'] = 'cache.backend.null';

// Override our search indexes to use the Solr server in our Lando setuo.
$config['search_api.index.product_catalog_en']['server'] = 'solr_lando';

$config['environment_indicator.indicator']['bg_color'] = '#63825e';
$config['environment_indicator.indicator']['fg_color'] = '#fff';
$config['environment_indicator.indicator']['name'] = 'Local development';

// Set language negotiation in our local Lando environment to be based on
// domains.
$config['language.negotiation']['url']['source'] = 'domain';
$config['language.negotiation']['url']['domains'] = [
  'en' => 'en.local.sims-ge-appliances.rbits',
  'fr' => 'fr.local.sims-ge-appliances.rbits',
];

// Disable the Google Tag Manager container.
$config['google_tag.container.ge_tag_manager']['status'] = FALSE;
