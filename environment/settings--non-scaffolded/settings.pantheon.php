<?php

/**
 * @file
 * Settings applied to our Pantheon cloud environments.
 */

// Read more about our how secrets are managed in the project in our README:
// https://github.com/relevantbits/sims-geappliances#secret-management.
$secrets = NULL;
$secrets_json = file_get_contents(__DIR__ . '/../files/private/secrets.json');
if ($secrets_json) {
  $secrets = json_decode($secrets_json, TRUE);
}

// Set language negotiation in the dev/test/live environments to be based on
// domains. Our feature branch environments can't have multiple domains
// assigned to them, so they will use path prefixes (/en and /fr) which is the
// exported config.
if (in_array(getenv('PANTHEON_ENVIRONMENT'), ['dev', 'test'])) {
  $config['language.negotiation']['url']['source'] = 'domain';
  $config['language.negotiation']['url']['domains'] = [
    'en' => sprintf('en.%s.sims-ge-appliances.rbits.io', getenv('PANTHEON_ENVIRONMENT')),
    'fr' => sprintf('fr.%s.sims-ge-appliances.rbits.io', getenv('PANTHEON_ENVIRONMENT')),
  ];
}
if (getenv('PANTHEON_ENVIRONMENT') == 'live') {
  $config['language.negotiation']['url']['source'] = 'domain';
  $config['language.negotiation']['url']['domains'] = [
    'en' => 'www.geappliances.ca',
    'fr' => 'www.electromenagersge.ca',
  ];
}

// Disable the Google Tag Manager container everywhere but our test and live
// environments.
if (!in_array(getenv('PANTHEON_ENVIRONMENT'), ['test', 'live'])) {
  $config['google_tag.container.ge_tag_manager']['status'] = FALSE;
}

// Configure Solr connections.
$solr_env = getenv('PANTHEON_ENVIRONMENT') == 'live' ? 'live' : 'dev';

$config['search_api.server.solr_content']['backend_config']['connector'] = 'basic_auth';
$config['search_api.server.solr_content']['backend_config']['connector_config']['scheme'] = 'https';
$config['search_api.server.solr_content']['backend_config']['connector_config']['host'] = 'us-va-8.solrcluster.com';
$config['search_api.server.solr_content']['backend_config']['connector_config']['port'] = '443';
$config['search_api.server.solr_content']['backend_config']['connector_config']['core'] = $solr_env . '_content';
$config['search_api.server.solr_content']['backend_config']['connector_config']['username'] = $secrets['solr_' . $solr_env . '_content_username'];
$config['search_api.server.solr_content']['backend_config']['connector_config']['password'] = $secrets['solr_' . $solr_env . '_content_password'];
// Adding site hash allows us to use this core from multiple environments.
$config['search_api.server.solr_content']['backend_config']['site_hash'] = $solr_env == 'dev';

$config['search_api.server.solr_products']['backend_config']['connector'] = 'basic_auth';
$config['search_api.server.solr_products']['backend_config']['connector_config']['scheme'] = 'https';
$config['search_api.server.solr_products']['backend_config']['connector_config']['host'] = 'us-va-8.solrcluster.com';
$config['search_api.server.solr_products']['backend_config']['connector_config']['port'] = '443';
$config['search_api.server.solr_products']['backend_config']['connector_config']['core'] = $solr_env . '_products';
$config['search_api.server.solr_products']['backend_config']['connector_config']['username'] = $secrets['solr_' . $solr_env . '_products_username'];
$config['search_api.server.solr_products']['backend_config']['connector_config']['password'] = $secrets['solr_' . $solr_env . '_products_password'];
// Adding site hash allows us to use this core from multiple environments.
$config['search_api.server.solr_products']['backend_config']['site_hash'] = $solr_env == 'dev';


// Configure environment indicator.
$config['environment_indicator.indicator']['fg_color'] = '#fff';
switch (getenv('PANTHEON_ENVIRONMENT')) {
  case 'dev':
    $config['environment_indicator.indicator']['bg_color'] = '#103b62';
    $config['environment_indicator.indicator']['name'] = 'Dev site';
    break;

  case 'test':
    $config['environment_indicator.indicator']['bg_color'] = '#c54b25';
    $config['environment_indicator.indicator']['name'] = 'Test site';
    break;

  case 'live':
    $config['environment_indicator.indicator']['bg_color'] = '#652713';
    $config['environment_indicator.indicator']['name'] = 'Live site';
    break;

  default:
    $config['environment_indicator.indicator']['bg_color'] = '#103b62';
    $config['environment_indicator.indicator']['name'] = 'Feature branch dev site';
    break;
}
