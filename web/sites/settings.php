<?php

/**
 * Load services definition file.
 */
$settings['container_yamls'][] = __DIR__ . '/services.yml';

$databases['default']['default'] = [
  'database' => getenv('MYSQL_DATABASE'),
  'driver' => 'mysql',
  'host' => getenv('MYSQL_HOSTNAME'),
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'password' => getenv("MYSQL_PASSWORD"),
  'port' => getenv('MYSQL_PORT'),
  'prefix' => '',
  'username' => getenv('MYSQL_USER'),
];

/**
 * SMTP configuration.
 */
if (getenv('MAIL_DRIVER') == 'smtp') {
  $config['smtp.settings']['smtp_on'] = 'on';
}
$config['smtp.settings']['smtp_host'] = getenv('MAIL_HOST');
$config['smtp.settings']['smtp_port'] = getenv('MAIL_PORT');
$config['smtp.settings']['smtp_protocol'] = getenv('MAIL_ENCRYPTION') ? getenv('MAIL_ENCRYPTION') : 'standard';
$config['smtp.settings']['smtp_username'] = getenv('MAIL_USERNAME');
$config['smtp.settings']['smtp_password'] = getenv('MAIL_PASSWORD');

/**
 * Move config sync directory outside the webroot.
 * We don't need any other confif dir, because environment based condigurations
 * are handled by config_split module.
 */
$settings["config_sync_directory"] = dirname(DRUPAL_ROOT) . '/config/sync';

/**
 * Private file path:
 *
 * A local file system path where private files will be stored. This directory
 * must be absolute, outside of the Drupal installation directory and not
 * accessible over the web.
 *
 * Note: Caches need to be cleared when this value is changed to make the
 * private:// stream wrapper available to the system.
 *
 * See https://www.drupal.org/documentation/modules/file for more information
 * about securing private files.
 */
$settings['file_private_path'] = dirname(DRUPAL_ROOT) . '/private-files';

/**
 * Set up environment indicator colors. @todo: Figure out the proper colors.
 * If you use your own settings.local.php, you can override them.
 */
$config['environment_indicator.indicator']['bg_color'] = '#ffffff';
$config['environment_indicator.indicator']['fg_color'] = '#000000';
$config['environment_indicator.indicator']['name'] = 'local';

/**
 * Momo environment based settings.
 */
if (isset($_ENV['ENVIRONMENT'])) {
  switch ($_ENV['ENVIRONMENT']) {
    case 'dev':
      $config['environment_indicator.indicator']['bg_color'] = '#008b49';
      $config['environment_indicator.indicator']['fg_color'] = '#ffffff';
      $config['environment_indicator.indicator']['name'] = 'dev';
      break;
    case 'stage':
      $config['environment_indicator.indicator']['bg_color'] = '#ddffdd';
      $config['environment_indicator.indicator']['fg_color'] = '#000000';
      $config['environment_indicator.indicator']['name'] = 'test';
      break;
    case 'live':
      $config['environment_indicator.indicator']['bg_color'] = '#da251c';
      $config['environment_indicator.indicator']['fg_color'] = '#ffffff';
      $config['environment_indicator.indicator']['name'] = 'live';
      break;
    default:
      $config['environment_indicator.indicator']['bg_color'] = '#ffffff';
      $config['environment_indicator.indicator']['fg_color'] = '#000000';
      $config['environment_indicator.indicator']['name'] = 'Unrecognized environment!';
      break;
  }
}

$settings['hash_salt'] = getenv('DRUPAL_HASH_SALT');
$settings['file_temp_path'] = '/tmp';

$settings['trusted_host_patterns'] = array(
	'^tag\.momentum\.hu$',
	'^dev4\.momentum\.hu$',
);

// Good to know #0: https://git.drupalcode.org/project/memcache/blob/8.x-2.x/README.txt
// Good to know #1: https://docs.acquia.com/acquia-cloud/performance/memcached/intro/
// Good to know #2: https://dropsolid.com/en/blog/memcache-drupal-8
// $settings['memcache']['servers'] = ['momcached.628dfr.cfg.euw1.cache.amazonaws.com:11211' => 'default'];
//$settings['memcache']['bins'] = ['default' => 'default'];
//$settings['memcache']['key_prefix'] = 'tagmomohu';

// Use memcache as the default bin.
// $settings['cache']['default'] = 'cache.backend.memcache';

// Override default fastchained backend for static bins.
// @see https://www.drupal.org/node/2754947
//$settings['cache']['bins']['bootstrap'] = 'cache.backend.memcache';
//$settings['cache']['bins']['config'] = 'cache.backend.memcache';
//$settings['cache']['bins']['discovery'] = 'cache.backend.memcache';
//$settings['cache']['bins']['dynamic_page_cache'] = 'cache.backend.memcache';
//$settings['cache']['bins']['render'] = 'cache.backend.memcache';


/**
 * If there is a local settings file, then include it.
 */
$local_settings = dirname(DRUPAL_ROOT) . "/settings.local.php";
if (file_exists($local_settings)) {
  include_once $local_settings;
}

$databases['default']['default'] = array (
  'database' => 'aboveh',
  'username' => 'szantog_devel',
  'password' => '123456',
  'prefix' => '',
  'host' => 'localhost',
  'port' => '3306',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
);
$settings['hash_salt'] = 'U3QLQ842L0lDSpbnCwJKn-D2962OMH1yqJtwUgqH3xATJkYcyIjmdlaBAC_ZMCttWrpgLrlUPg';
