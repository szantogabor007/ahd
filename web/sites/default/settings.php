<?php

include dirname(DRUPAL_ROOT) . '/environment/settings--non-scaffolded/settings.common.php';

/**
 * If there is a local settings file, then include it.
 */
$local_settings = dirname(DRUPAL_ROOT) . "/settings.local.php";
if (file_exists($local_settings)) {
  include_once $local_settings;
}
